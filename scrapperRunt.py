# -*- coding: utf-8 -*-
"""
Created on Tue Oct 23 10:30:14 2018

@author: jabuitra
"""

import re, csv
from time import sleep, time
from random import uniform, randint
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException 
from selenium import webdriver
import time

#chrome_options = Options()
#chrome_options.add_argument("--headless")
#driver = webdriver.Chrome(chrome_options=chrome_options)


driver = webdriver.Chrome(r'D:\para_copiar\chromedriver\chromedriver.exe')
#driver = webdriver.Firefox(executable_path=r'D:\geckodriver.exe')
#driver = webdriver.PhantomJS(r'D:\phantomjs\phantomjs.exe')
driver.get("https://www.runt.com.co/consultaCiudadana/#/consultaVehiculo")
time.sleep(2)        
   
mainWin = driver.current_window_handle  
        
v_Vin='LJ12EKR21H4001801'

# obtiene la lista en la cual se puede seleccionar porque atributos buscar los datos, en este caso es con el Vin
obtenerListDesplegable = driver.find_element_by_id('tipoConsulta')
time.sleep(1)

#da click en la lista seleccionada
obtenerListDesplegable.click()
time.sleep(1)
#seleccciona la opcion correcta del vin

listaOpciones = obtenerListDesplegable.find_elements_by_tag_name('option')
time.sleep(1)
listaOpciones[1].click()
time.sleep(1)

# Envia parametro
driver.find_element_by_id('vin').send_keys(v_Vin)
time.sleep(1)

driver.switch_to_frame(driver.find_elements_by_tag_name("iframe")[0])
driver.save_screenshot('test.png')
print(driver.page_source)
CheckBox = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.ID ,"recaptcha-anchor"))
        ) 

def wait_between(a,b):
	rand=uniform(a, b) 
	sleep(rand)
    
wait_between(0.5, 0.7)  
CheckBox.click()

